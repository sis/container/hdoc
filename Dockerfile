ARG BASE_IMAGE=ubuntu:lunar
FROM $BASE_IMAGE as build
ARG VERSION=1.4.1

RUN apt-get update && \
    apt-get install -y \
        'g++' \
        meson \
        cmake \
        ninja-build \
        pkg-config \
        llvm \
        clang \
        clang-c++ \
        xxd \
        libssl-dev \
        libclang-dev \
        curl \
        python3-certifi \
    && rm -rf /var/lib/apt/lists/*

RUN curl "https://codeload.github.com/hdoc/hdoc/tar.gz/refs/tags/${VERSION}" | tar -xvz
RUN cd hdoc-* \
  && meson setup build \
  && ninja -C build \
  && DESTDIR=/target meson install -C build
COPY ./deb /deb
RUN sed -i "s/^Version: .*/Version: ${VERSION}/" /deb/DEBIAN/control \
  && chmod 755 /deb/DEBIAN \
  && mkdir -p /deb/usr/local/bin/ \
  && cp /target/usr/local/bin/hdoc /deb/usr/local/bin/ \
  && dpkg -b /deb /hdoc.deb

# deb stage
FROM scratch as deb
COPY --from=build /hdoc.deb /

# final container stage
FROM $BASE_IMAGE
COPY --from=build /target/usr/local/bin/hdoc /usr/local/bin
RUN apt-get update && \
    apt-get install -y \
        libllvm15 \
        libclang-cpp15 \
        openssl \
    && rm -rf /var/lib/apt/lists/*
