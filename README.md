# hdoc - container and deb

This repo builds containers and deb files for [hdoc](https://hdoc.io/).

## deb files

Operating System | Download
---|---
![Ubuntu Jammy](
https://assets.ubuntu.com/v1/ff6a9a38-ubuntu-logo-2022.svg){height=20px}
Jammy | [hdoc.deb](../-/jobs/artifacts/main/raw/hdoc.deb?job=deb)
